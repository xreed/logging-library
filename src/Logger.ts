/**
 *
 * <h1 style="text-decoration: underline">Introduction</h1>
 * Logger is a typescript library for logging (ie printing) any text message
 * (<code>message</code>) to the browser console.
 * You can define what type of severity level (<code>sevLevel</code>)
 * you want and dont want to log.
 *
 * <section>
 * <h1 style="text-decoration: underline">Usage</h1>
 * <pre>
 * import { Logger } from "./Logger";
 * const log1 = new Logger();
 * log1.logMessage("test1", "info"); // "test1"
 * </pre>
 * </section>
 *
 * <section>
 * <h1 style="text-decoration: underline">Severity Levels</h1>
 * <p>There are 4 severity levels (<code>sevLevel</code>) you
 * can use for <code>logMessage</code> method:</p>
 * <ol>
 * <li><code>info</code></li>
 * <li><code>warn</code></li>
 * <li><code>error</code></li>
 * <li><code>debug</code></li>
 * </ol>
 *
 * <h3>Info</h3>
 * <p><code>info</code> is the informational severity level.
 * It prints text message with console.info()</p>
 * <h3>Warn</h3>
 * <p><code>warn</code> is the warning severity level.
 * It prints text message with console.warn()</p>
 * <h3>Error</h3>
 * <p><code>error</code> is the error severity level.
 * It prints text message with console.error()</p>
 * <h3>Debug</h3>
 * <p><code>debug</code> is the debug severity level.
 * It prints text message with console.debug()</p>
 * </section>
 *
 *
 * @module
 */

/**
 * Prints a message of your choice to the browser console
 */
export class Logger {
    /**
     * Ignores Error severity level messages if true.
     */
    ignoreError: boolean;
    /**
     * Ignores Warning severity level messages if true.
     */
    ignoreWarn: boolean;
    /**
     * Ignores Information severity level messages if true.
     */
    ignoreInfo: boolean;
    /**
     *Ignores Debugging severity level messages if true.
     */
    ignoreDebug: boolean;
    /**
     * Ignores all severity levels if true.
     */
    ignoreAll: boolean;

    /**
     * Initializes the Logger object with default values of false.
     */
    constructor() {
        this.ignoreError = false;
        this.ignoreWarn = false;
        this.ignoreInfo = false;
        this.ignoreDebug = false;
        this.ignoreAll = false;
    }

    /**
     * Logs a message to the browser console.
     * @param message -- The message you want to print to the browser console
     * @param sevLevel -- The severity level of the message you want to print
     * ("info", "warn", "error", "debug")
     * @returns -- Void, since it prints to the browser console
     */
    logMessage(message: string, sevLevel: string): void {
        if (sevLevel === "error" && ! this.ignoreError)
            console.error(message);
        else if (sevLevel === "warn" && ! this.ignoreWarn)
            console.warn(message);
        else if (sevLevel === "info" && ! this.ignoreInfo)
            console.info(message);
        else if (sevLevel === "debug" && ! this.ignoreDebug)
            console.debug(message);
    }

    /**
     * Switches on/off the severity level you want to ignore.
     * @param sevLevel -- The severity level you want/dont want to ignore.
     * Type "all" to ignore all. Or, type "none" to stop ignoring all,
     * resetting them to their default values (false).
     */
    setIgnore(sevLevel: string): void {
        switch (sevLevel) {
            case "error":
                this.ignoreError = ! this.ignoreError;
                break;
            case "warn":
                this.ignoreWarn = ! this.ignoreWarn;
                break;
            case "info":
                this.ignoreInfo = ! this.ignoreInfo;
                break;
            case "debug":
                this.ignoreDebug = ! this.ignoreDebug;
                break;
            case "all":
                this.ignoreAll = true;
                this.ignoreError = true;
                this.ignoreWarn = true;
                this.ignoreInfo = true;
                this.ignoreDebug = true;
                break;
            case "none":
                this.ignoreAll = false;
                this.ignoreError = false;
                this.ignoreWarn = false;
                this.ignoreInfo = false;
                this.ignoreDebug = false;
                break;
        }
    }

    /**
     * A prompt that simply prints what was ignored
     * @param message -- The message that was ignored, displayed
     * back to the user.
     */
    ignorePrompt(message: string): void {
        console.log("######################");
        console.log("THE FOLLOWING MESSAGE WAS IGNORED:");
        console.log(message);
        console.log();
        this.getIgnore();
        console.log("######################");
    }

    /**
     * Prints what type of severity level you are ignoring back to user.
     */
    getIgnore(): void {
        console.log("CURRENTLY IGNORING:");

        if (this.ignoreAll) {
            console.log("IGNORING ALL");
        } else {
            if (this.ignoreWarn)
                console.log("WARN");
            if (this.ignoreError)
                console.log("ERROR");
            if (this.ignoreInfo)
                console.log("INFO");
            if (this.ignoreDebug)
                console.log("DEBUG");
        }
    }
}